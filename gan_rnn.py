﻿import pandas as pd
import numpy as np
import math
import random
from tqdm import tqdm
import keras
from keras.models import Sequential  
from keras.layers.core import Dense, Activation  
from keras.layers.recurrent import LSTM
import os
import soundfile as sf
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from keras.optimizers import Adam
import matplotlib.pyplot as plt
#ハイパーパラメータ
filepath = "Kick"
in_out_neurons = 1
hidden_neurons = 200
step = 500
epoch = 1
batch = 100


def normalize(data):
    vmin = np.amin(data)
    vmax = np.amax(data)
    data = (data - vmin).astype(float) / (vmax - vmin).astype(float) * 2 -1
    return data

def waveload(filepath):
    files = os.listdir(filepath)
    sound_data = []
    sr = 44100
    for file in enumerate(files[:60]):
        try:
            
            path = filepath + "\\" + file[1]
            data, samplerate = sf.read(path)
            if samplerate == 44100:
                sound_data.append(normalize(data.T[0]))
                
        except:
            print(str(file) + "をロードできませんでした")
    print(str(len(sound_data)) +"のファイルをロードしました。")
    print("サンプリングレートは" + str(sr) + "です")
    return sound_data

def make_testdata(filepath):
    data, samplerate = sf.read(filepath)
    data = normalize(data.T[0])
    X_arr = []
    X_arr.append(data[:step])
    X_data = np.asarray(X_arr)[:,:,np.newaxis]
    return X_data
    
def remake_testdata(data,val):
    data = np.delete(data,0,1)
    data = np.insert(data,199,val,1)
    return data

#データ作成
def make_data(data):
    X_arr = []
    Y_arr = []
    print("Normalizing data : Step=" + str(step))
    for i,file in enumerate(tqdm(data[:40])):
        length = file.shape[0]
        #ファイルを保存
        for i in range(length-step -1):
            X_arr.append(file[i:i + step])
            Y_arr.append(file[i+step+1])
            # plt.plot(file[i:i + step])
            # plt.show()
            # plt.clf()
    
    print("Transrate numpy")
    X_data = np.asarray(X_arr)[:,:,np.newaxis]
    Y_data = np.asarray(Y_arr)[:,np.newaxis]
    print("Result : Step=" + str(step) +"  Data count=" + str(X_data.shape[0]))
    return X_data,Y_data,

def Memory():
    sound_data = waveload(filepath)
    X_train,Y_train = make_data(sound_data)
    return train_test_split(X_train, Y_train, test_size=int(len(X_train) * 0.5))


x, val_x, y, val_y = Memory()
pred_data = make_testdata("Kick/Kick78.wav")

act = keras.layers.advanced_activations.LeakyReLU(alpha=0.2)
model = Sequential()  
model.add(LSTM(hidden_neurons, batch_input_shape=(None, step, in_out_neurons), return_sequences=False))  
model.add(Dense(in_out_neurons))  
model.add(act)  
model.compile(loss="mean_squared_error", optimizer=Adam(lr=0.001, beta_1=0.9, beta_2=0.999),metrics=['accuracy'])
# early_stopping = EarlyStopping(monitor='val_loss', patience=2)

model.fit(x,y, batch_size=batch, epochs=epoch, validation_split=0.25) 

inputlen = 2000

result_data = pred_data.reshape(200).tolist()
for i in tqdm(range(inputlen)):
    p = model.predict(pred_data)
    pred_data = remake_testdata(pred_data,p)
    result_data.append(p)

plt.plot(result_data)
plt.show()